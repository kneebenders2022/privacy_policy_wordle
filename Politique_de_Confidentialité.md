**Politique de confidentialité**

Julien Jollivet et François Plumerault ont conçu l'application The WORD comme une application financée par la publicité. Ce SERVICE est fourni par Julien Jollivet & François Plumerault à titre gracieux et est destiné à être utilisé tel quel.

Cette page est utilisée pour informer les visiteurs de ma politique en matière de collecte, d'utilisation et de divulgation des informations personnelles si quelqu'un décide d'utiliser mon service.

Si vous choisissez d'utiliser mon service, vous acceptez la collecte et l'utilisation des informations en relation avec cette politique. Les informations personnelles que je collecte sont utilisées pour fournir et améliorer le service. Je n'utiliserai ni ne partagerai vos informations avec quiconque, sauf dans les cas décrits dans la présente politique de confidentialité.

Les termes utilisés dans cette Politique de Confidentialité ont la même signification que dans nos Termes et Conditions, qui sont accessibles à The WORD, sauf définition contraire dans cette Politique de Confidentialité.

**Collecte et utilisation des informations**

Pour une meilleure expérience, lors de l'utilisation de notre Service, je peux vous demander de nous fournir certaines informations personnellement identifiables, y compris mais non limité à IP, logs, activité, localisation approximative. Les informations que je demande seront conservées sur votre appareil et ne sont en aucun cas collectées par moi.

L'application utilise des services tiers qui peuvent recueillir des informations permettant de vous identifier.

Lien vers la politique de confidentialité des fournisseurs de services tiers utilisés par l'application

* [Google Play Services](https://www.google.com/policies/privacy/)
* [AdMob](https://support.google.com/admob/answer/6128543?hl=en)
* [Google Analytics pour Firebase](https://firebase.google.com/policies/analytics)
* [Firebase Crashlytics](https://firebase.google.com/support/privacy/)

**Données de journal**

Je tiens à vous informer que chaque fois que vous utilisez mon Service, en cas d'erreur dans l'application, je collecte des données et des informations (par le biais de produits tiers) sur votre téléphone appelées Données de journal. Ces données peuvent inclure des informations telles que l'adresse du protocole Internet ("IP") de votre appareil, le nom de l'appareil, la version du système d'exploitation, la configuration de l'application lors de l'utilisation de mon service, l'heure et la date de votre utilisation du service et d'autres statistiques.

**Cookies**

Les cookies sont des fichiers contenant une petite quantité de données qui sont généralement utilisés comme identifiants uniques anonymes. Ils sont envoyés à votre navigateur à partir des sites web que vous visitez et sont stockés dans la mémoire interne de votre appareil.

Ce service n'utilise pas explicitement ces "cookies". Cependant, l'application peut utiliser du code et des bibliothèques de tiers qui utilisent des "cookies" pour collecter des informations et améliorer leurs services. Vous avez la possibilité d'accepter ou de refuser ces cookies et de savoir quand un cookie est envoyé à votre appareil. Si vous choisissez de refuser nos cookies, il se peut que vous ne puissiez pas utiliser certaines parties de ce service.

**Fournisseurs de services**

Je peux employer des sociétés et des personnes tierces pour les raisons suivantes :

* Pour faciliter notre Service ;
* Pour fournir le Service en notre nom ;
* Pour effectuer des services liés au Service ; ou
* Pour nous aider à analyser la façon dont notre service est utilisé.

Je tiens à informer les utilisateurs de ce service que ces tiers ont accès à leurs informations personnelles. La raison en est l'exécution des tâches qui leur sont confiées en notre nom. Toutefois, ils sont tenus de ne pas divulguer ou utiliser ces informations à d'autres fins.

**Sécurité**

J'apprécie la confiance que vous nous accordez en nous fournissant vos informations personnelles, et nous nous efforçons donc d'utiliser des moyens commercialement acceptables pour les protéger. Mais n'oubliez pas qu'aucune méthode de transmission sur Internet ou de stockage électronique n'est sûre et fiable à 100 % et que je ne peux pas garantir une sécurité absolue.

**Liens vers d'autres sites**

Ce service peut contenir des liens vers d'autres sites. Si vous cliquez sur un lien tiers, vous serez dirigé vers ce site. Notez que ces sites externes ne sont pas exploités par moi. Par conséquent, je vous conseille vivement de consulter la politique de confidentialité de ces sites. Je n'ai aucun contrôle et n'assume aucune responsabilité quant au contenu, aux politiques de confidentialité ou aux pratiques des sites ou services tiers.

**Confidentialité des enfants**

Ces services ne s'adressent pas aux personnes âgées de moins de 13 ans. Je ne collecte pas sciemment d'informations personnellement identifiables auprès d'enfants de moins de 13 ans. Dans le cas où je découvre qu'un enfant de moins de 13 ans m'a fourni des informations personnelles, je les supprime immédiatement de nos serveurs. Si vous êtes un parent ou un tuteur et que vous savez que votre enfant nous a fourni des informations personnelles, veuillez me contacter afin que je puisse prendre les mesures nécessaires.

**Modifications de la présente politique de confidentialité

Je peux mettre à jour notre politique de confidentialité de temps à autre. Il vous est donc conseillé de consulter régulièrement cette page pour prendre connaissance des changements éventuels. Je vous informerai de tout changement en publiant la nouvelle politique de confidentialité sur cette page.

Cette politique est en vigueur à compter du 2022-04-03.

**Nous contacter**

Si vous avez des questions ou des suggestions concernant ma politique de confidentialité, n'hésitez pas à me contacter à l'adresse feedback.kneebenders@gmail.com.

Cette page de politique de confidentialité a été créée sur [privacypolicytemplate.net](https://privacypolicytemplate.net) et modifiée/générée par [App Privacy Policy Generator](https://app-privacy-policy-generator.nisrulz.com/).
